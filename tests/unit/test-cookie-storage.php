<?php namespace EM\Tests\Unit;

use EM\Coupons\Autoloadable_Coupon;
use EM\Storage\Cookie_Storage;

class Test_Cookie_Storage extends \PHPUnit\Framework\TestCase {

	public function setUp() {
		\WP_Mock::setUp();
	}

	public function tearDown() {
		\WP_Mock::tearDown();
	}

	/**
	 * @test
	 */
	public function get_one_by_code() {
		$id      = mt_rand( 1, 20 );
		$code    = 'SOME_STRING';
		$is_used = true;

		$coupon_in_cookie          = new \stdClass();
		$coupon_in_cookie->id      = $id;
		$coupon_in_cookie->code    = $code;
		$coupon_in_cookie->is_used = $is_used;

		$cookie_session_data = [ $coupon_in_cookie ];

		$this->mock_wc_factory_with_session( $cookie_session_data );

		$coupon  = new Autoloadable_Coupon( null, $code, false );
		$subject = new Cookie_Storage();
		$result  = $subject->get_one_by_code( $code );
		$this->assertEquals( $coupon->get_code(), $result->get_code() );
	}

	/**
	 * @test
	 */
	public function no_coupon_found() {
		$code = 'SOME_STRING';

		$this->mock_wc_factory_with_session( [] );

		$subject = new Cookie_Storage();
		$result  = $subject->get_one_by_code( $code );
		$this->assertEquals( null, $result );
	}

	/**
	 * @param $cookie_session_data
	 */
	private function mock_wc_factory_with_session( $cookie_session_data ) {
		$wc_session = $this->getMockBuilder( \WC_Session::class )
		                   ->disableOriginalConstructor()
		                   ->setMethods( [ 'get' ] )
		                   ->getMock();

		$wc_session->method( 'get' )->with( Cookie_Storage::AUTOLOADABLE_COUPON_COOKIE )
		           ->willReturn( json_encode( $cookie_session_data ) );

		$wc = $this->getMockBuilder( \WooCommerce::class )
		           ->disableOriginalConstructor()
		           ->getMock();

		$wc->session = $wc_session;

		\WP_Mock::userFunction( 'WC', array(
			'return' => $wc,
		) );
	}
}

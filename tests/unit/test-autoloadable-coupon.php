<?php namespace EM\Tests\Unit;

use \EM\Coupons\Autoloadable_Coupon;
use Exception;
use WC_Discounts;

class Test_Autoloadable_Coupon extends \PHPUnit\Framework\TestCase {

	public function setUp() {
		\WP_Mock::setUp();
	}

	public function tearDown() {
		\WP_Mock::tearDown();
	}

	/**
	 * @test
	 */
	public function it_checks_properly_if_is_used() {
		$this->mock_wc_instance_with_cart( [] );

		$id      = mt_rand( 1, 10 );
		$code    = 'SOME_COUPON_CODE';
		$is_used = true;
		$coupon  = new Autoloadable_Coupon( $id, $code, $is_used );
		$this->assertEquals( true, $coupon->is_used(), 'Coupon was used based on constructor parameters.' );
	}

	/**
	 * @test
	 */
	public function it_checks_coupon_usage_from_wc() {
		$coupon = $this->make_unused_autoloadable_coupon();
		$this->mock_wc_instance_with_cart( [ $coupon->get_code() ] );

		$this->assertEquals( true, $coupon->is_used(), 'Coupon was used based on WC data.' );
	}

	/**
	 * @test
	 */
	public function coupon_is_not_used() {
		$coupon = $this->make_unused_autoloadable_coupon();
		$this->mock_wc_instance_with_cart( [ 'SOME_OTHER_CODE' ] );

		$this->assertEquals( false, $coupon->is_used(), 'Coupon should not be used.' );
	}

	/**
	 * @test
	 * @dataProvider is_applicable_cases
	 */
	public function is_applicable( $is_coupon_valid, $applied_coupons, $expected ) {
		$this->mock_wc_instance_with_cart( $applied_coupons );

		\WP_Mock::userFunction( 'is_wp_error', array(
			'return' => false,
		) );

		$subject   = $this->make_unused_autoloadable_coupon();
		$wc_coupon = $this->getMockBuilder( \WC_Coupon::class )
		                  ->disableOriginalConstructor()
		                  ->getMock();

		\WP_Mock::userFunction( 'make_wc_coupon', array(
			'args'   => $subject->get_code(),
			'return' => $wc_coupon,
		) );

		$discounts = $this->getMockBuilder( WC_Discounts::class )
		                  ->disableOriginalConstructor()
		                  ->setMethods( [ 'is_coupon_valid' ] )
		                  ->getMock();

		$discounts->method( 'is_coupon_valid' )->with( $wc_coupon )->will( $is_coupon_valid );

		\WP_Mock::userFunction( 'make_wc_discounts', array(
			'return' => $discounts,
		) );

		$this->assertEquals( $expected, $subject->is_applicable() );
	}

	public function is_applicable_cases() {
		return [
			[ $this->returnValue( true ), [], true ],
			[ $this->returnValue( false ), [], false ],
			[ $this->returnValue( true ), [ 'SOME_COUPON_CODE' ], false ],
			[ $this->returnValue( true ), [ 'SOME_COUPON_CODE1' ], true ],
			[ $this->throwException( new Exception() ), [ 'SOME_COUPON_CODE' ], false ],
		];
	}

	/**
	 * @return Autoloadable_Coupon
	 */
	private function make_unused_autoloadable_coupon() {
		$id      = mt_rand( 1, 10 );
		$code    = 'SOME_COUPON_CODE';
		$is_used = false;
		$coupon  = new Autoloadable_Coupon( $id, $code, $is_used );

		return $coupon;
	}

	private function mock_wc_instance_with_cart( $applied_coupons ) {
		$wc_cart = $this->getMockBuilder( \WC_Cart::class )
		                ->disableOriginalConstructor()
		                ->setMethods( [ 'get_applied_coupons' ] )
		                ->getMock();

		$wc_cart->expects( $this->any() )->method( 'get_applied_coupons' )->willReturn( $applied_coupons );

		$wc = $this->getMockBuilder( \WooCommerce::class )
		           ->disableOriginalConstructor()
		           ->getMock();

		$wc->cart = $wc_cart;

		\WP_Mock::userFunction( 'WC', array(
			'return' => $wc,
		) );
	}
}

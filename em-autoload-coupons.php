<?php
/**
 * Plugin Name: Auoloadable Coupons for WooCommerce
 * Plugin URI: https://github.com/EduardMaghakyan/em-autoload-coupons
 * Description: Autoload coupon codes from URL.
 * Version: 1.0.0
 * Author: Eduard Maghakyan
 * Author URI:https://github.com/EduardMaghakyan
 * Developer: Eduard Maghakyan
 * Developer URI: https://github.com/EduardMaghakyan
 *
 * WC requires at least: 3.0
 * WC tested up to: 3.2.5
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'EM_AUTOLOAD_COUPONS_PATH', __DIR__ . '/' );
define( 'EM_AUTOLOAD_COUPONS_URL', plugin_dir_url( __FILE__ ) . '/' );

// Require composer autoloader.
require_once EM_AUTOLOAD_COUPONS_PATH . 'vendor/autoload.php';

/**
 * Begins execution of the plugin.
 */
//function run_em_autoload_coupons() {
//	$cookie = new Cookie_Storage();
//	$plugin = new Autoload_Coupons_Controller( $cookie );
//	$plugin->add_hooks();
//
//	$coupon_link = new Coupon_Link();
//	$coupon_link->run();
//
//	/**
//	 * @TODO Move to appropriate file/folder
//	 */
//	add_action( 'admin_enqueue_scripts', function () {
//		wp_enqueue_script(
//			'clipboard_js',
//			EM_AUTOLOAD_COUPONS_URL . '/node_modules/clipboard/dist/clipboard.min.js',
//			[],
//			'1.7.1'
//		);
//	} );
//	/**
//	 * @TODO Move to appropriate file/folder
//	 */
//	add_action( 'admin_footer', function () {
//		?>
<!--		<script>-->
<!--          var clipboard = new Clipboard('.copy-coupon-link');-->
<!--          clipboard.on('success', function(e) {-->
<!--            alert('Autoloadbale coupon link is copied to clipboard!');-->
<!--          });-->
<!--          jQuery('.copy-coupon-link').click(function(e) {-->
<!--            e.preventDefault();-->
<!--          });-->
<!--		</script>-->
<!--		--><?php
//	});
//
//}

/**
 * Check if WooCommerce is active
 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
//	run_em_autoload_coupons();

}

add_action( 'init', function() {
//	WC()->session->set('em_autoload', '11111111111');
//	var_dump( WC()->session->get('em_autoload'));
//	die();
} );


/**
 * Create instance of WC_Coupon.
 *
 * @param $coupon_code
 *
 * @return WC_Coupon
 */
function make_wc_coupon( $coupon_code ) {
    return new WC_Coupon( $coupon_code );
}

/**
 * Create instance of WC_Discounts.
 *
 * @return WC_Discounts
 */
function make_wc_discounts() {
	return new WC_Discounts( WC()->cart );
}

<?php namespace EM\Coupons;

interface Coupon {
	public function get_id();

	public function get_code();

	public function is_used();
}

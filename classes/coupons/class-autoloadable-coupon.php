<?php namespace EM\Coupons;

use Exception;
use WC_Coupon;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Class Autoloadable_Coupon
 * @package EM\Coupons
 */
class Autoloadable_Coupon implements Coupon {

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $code;

	/**
	 * @var boolean
	 */
	private $is_used;

	public function __construct( $id, $code, $is_used ) {
		$this->id = $id;
		$this->code = $code;
		$this->is_used = $is_used;
	}

	/**
	 * @return int
	 */
	public function get_id() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function get_code() {
		return $this->code;
	}

	/**
	 * @return bool
	 */
	public function is_used() {
		return $this->is_used || in_array( $this->get_code(), WC()->cart->get_applied_coupons() );
	}

	/**
	 * Check if coupon is applicable to cart and hasn't been applied yet.
	 *
	 * @return bool
	 */
	public function is_applicable() {
		$the_coupon = make_wc_coupon( $this->get_code() );

		return $this->is_valid( $the_coupon ) && ! $this->is_used();
	}

	/**
	 * Validator function from WC source with minor adjustment.
	 *
	 * @param WC_Coupon $the_coupon
	 *
	 * @return bool
	 */
	private function is_valid( WC_Coupon $the_coupon ) {
		$discounts = make_wc_discounts();
		try {
			$valid = $discounts->is_coupon_valid( $the_coupon );
		} catch ( Exception $e ) {
			$valid = false;
		}

		if ( is_wp_error( $valid ) ) {
			$valid = false;
		}

		return $valid;
	}
}

<?php

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Class EM_Autoload_Coupons_Controller
 */
class Autoload_Coupons_Controller {

	/**
	 * URL parameter for coupon
	 */
	const EM_COUPON = 'wc_autoload_coupon';

	/**
	 * @var Cookie_Storage
	 */
	private $cookie;

	/**
	 * Wc_Autoload_Coupons constructor.
	 *
	 * @param Cookie_Storage $cookie
	 */
	public function __construct( Cookie_Storage $cookie ) {
		$this->cookie = $cookie;
	}

	/**
	 * Add actions.
	 */
	public function add_hooks() {
		add_action( 'template_redirect', [ $this, 'retrieve_coupon_from_url' ] );
		add_action( 'woocommerce_after_calculate_totals', [ $this, 'autoload_coupon' ] );
	}

	/**
	 * Main controller to apply coupon.
	 *
	 * @return bool
	 */
	public function autoload_coupon() {
		// Coupons are globally disabled.
		if ( ! wc_coupons_enabled() ) {
			return false;
		}

		// Sanitize coupon code.
		$coupon_code = wc_format_coupon_code( $this->cookie->get() );
		// Get the coupon.
		$autoloadable_coupon = new Autoloadable_Coupon( $coupon_code, false );
		if ( $autoloadable_coupon->is_applicable() ) {
			return WC()->cart->apply_coupon( $autoloadable_coupon->get_code() );
		}

		return false;
	}

	/**
	 * Retrieve coupon code from URL and store in cookies.
	 */
	public function retrieve_coupon_from_url() {
		if ( array_key_exists( self::EM_COUPON, $_GET ) ) {
			$this->cookie->set( sanitize_text_field( $_GET[ self::EM_COUPON ] ) );
		}
	}
}

<?php

class Coupon_Link {

	public function run() {
		add_filter( 'post_row_actions', [ $this, 'modify_list_row_actions' ], 10, 2 );
		add_filter( 'edit_form_after_title', [ $this, 'edit_form_after_title' ], 11, 1 );
	}

	public function edit_form_after_title( $post ) {
		if ( 'shop_coupon' === $post->post_type ) {
			?>
			<style>
				.input-group {
					display: table;
				}
				.input-group input {
					display: table-cell;
					position: relative;
					width: 100%;
					height: 31px;
				}
				.input-group-button {
					width: 1%;
					vertical-align: middle;
					display: table-cell;
				}
			</style>
			<div class="input-group">
				<input id="coupon-link" value="<?php echo esc_url( $this->get_coupon_link( $post ) ); ?>" type="text" readonly class="widefat">
				<span class="input-group-button">
					<button class="copy-coupon-link button button-large" data-clipboard-target="#coupon-link">Copy</button>
				</span>
			</div>
			<?php
		}
	}

	public function modify_list_row_actions( $actions, $post ) {
		if ( $post->post_type == "shop_coupon" ) {
			$copy_link = $this->get_coupon_link( $post );
			$actions   = array_merge( $actions, [
				'copy' => sprintf( '<a class="copy-coupon-link" data-clipboard-text="%1$s" href="#">%2$s</a>',
					esc_url( $copy_link ),
					'Sharable Link'
				)
			] );
		}

		return $actions;
	}

	/**
	 * @param $post
	 *
	 * @return string
	 */
	private function get_coupon_link( $post ) {
		$url       = get_home_url();
		$copy_link = add_query_arg( [ Autoload_Coupons_Controller::EM_COUPON => $post->post_title ], $url );

		return $copy_link;
	}
}

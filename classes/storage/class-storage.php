<?php namespace EM\Storage;

use EM\Coupons\Coupon;

abstract class Storage {

	public abstract function save( Coupon $coupon );

	public abstract function get_one_by_code( $code );
}

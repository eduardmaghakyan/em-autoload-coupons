<?php namespace EM\Storage;

use EM\Coupons\Autoloadable_Coupon;
use EM\Coupons\Coupon;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Class Em_Autoload_Coupons_Cookie
 */
class Cookie_Storage extends Storage {

	const AUTOLOADABLE_COUPON_COOKIE = 'em_autoload_coupon';

	public function save( Coupon $coupon ) {
	}

	public function get_one_by_code( $code ) {
		$coupons = json_decode( WC()->session->get( self::AUTOLOADABLE_COUPON_COOKIE ) );
		if ( empty( $coupons ) ) {
			return null;
		}

		return $this->find_coupon_object_by_code( $coupons, $code );
	}

	private function find_coupon_object_by_code( $coupons, $code ) {
		if ( empty( $coupons ) ) {
			return null;
		}

		$coupon = array_shift( $coupons );

		return $coupon->code === $code ? $this->make_coupon( $coupon ) : $this->find_coupon_object_by_code( $coupons, $code );
	}

	private function make_coupon( $coupon ) {
		return new Autoloadable_Coupon( $coupon->id, $coupon->code, $coupon->is_used );
	}
}

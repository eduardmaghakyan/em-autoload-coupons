<?php namespace EM\Storage;

/**
 * Class Autoloadable_Coupons
 */
class Autoloadable_Coupons {

	/**
	 * @var Storage
	 */
	private $storage;

	/**
	 * Autoloadable_Coupons constructor.
	 *
	 * @param Storage $storage
	 */
	public function __construct( Storage $storage ) {
		$this->storage = $storage;
	}

	/**
	 * Get all autoloadable coupons.
	 */
	public function get_all() {
		$coupons = [];
		$coupons_from_storage = $this->storage->get_all();
		if ( ! empty( $coupons_from_storage ) ) {
			foreach ( $coupons_from_storage as $coupon ) {
				$coupons[] = new Autoloadable_Coupon( $coupon->code, $coupon->used );
			}
		}

		return $coupons;
	}

	public function save(Autoloadable_Coupon $coupon) {
		$this->storage->save( $coupon );

		return true;
	}
}

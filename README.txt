=== Autoload WooCommerce Coupons ===
Plugin Name: Autoload WooCommerce Coupons
Author: Eduard Maghakyan
Author URI: https://github.com/EduardMaghakyan
Tags: woocommerce, coupons, autoload coupons
Requires at least: 3.0.1
Tested up to: 4.9.1
Stable tag: 1.0.0
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Autoload WooCommerce coupons directly from URL. Add `?wc_autoload_coupon=YOUCOUPONCODE` and it will be automatically applied to customers cart.

== Description ==
Easily share you coupon codes. Customers don't need to remember or copy/paste coupon codes.
Simply add `https://www.example.com?wc_autoload_coupon=YOUCOUPONCODE` to URL to your store and the code
`YOUCOUPONCODE` will automatically be applied to customers shopping cart.

== Installation ==

1. Upload `em-autoload-coupons` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 1.0.0 =
* Initial release of plugin.
